# corrector.js

Настраиваемый типограф с небольшим словарем дефолтных правил.

Для работы не требует сторонних библиотек.

## Поддерживаемые браузеры

IE8+ и все современные браузеры. В неподдерживаемых браузерах ничего не будет происходить даже при подключении библиотеки и использовании API (без ошибок).

## API

Библиотека добавляет в глобальную область единственный объект — класс ```Corrector```. Это конструктор, который в качестве переменных принимает два необязательных параметра:

* __selector__ — селектор элемента (или элементов), для текста которого необходима работа типографа, по умолчанию ```body```.
* __rules__ — объект, расширяющий дефолтный словарь правил (об этом ниже).

### Методы:

* __run()__ — запускает типограф. После вызова этого метода содержимое всех текстовых дочерних нод будет исправлено в соот-ии с правилами словаря.
* __addRules(rules {Object})__ — расширяет словарь новыми правилами.
* __clearRules([useDefault {Boolean}])__ — очищает словарь. Если передать ```true``` в качестве аргумента очищенный словарь будет дополнен правилами по умолчанию.

### Словарь и правила

Словарь — это объект, ключами которого являются краткие названия правила, а значениями — массив двух аргументов для метода replace. Простой пример словаря, который заменяет все некоректные дефисы на тире:

```
{
	_dashes: [/\s—/g, String.fromCharCode(160) + '—'],
}
```

### События

При подключенной библиотеке jQuery типограф инициализирует два события для объекта ```window```: ```corrector-start``` и ```corrector-end```. При начале и окончании работы типографа соот-но.

### Примеры

```Corrector``` — самовызывающийся конструктор, допустимо его использовать без ```new```.

Запуск типографа для всего документа с дефолными правилами:

```
Corrector().run();
```

Запуск типографа с расширенным словарем правил для элемента ```#badtext```:

```
Corrector('#badtext', {
	zap: [/\s(зао)\s/ig, ' ЗАО ']
}).run();
```