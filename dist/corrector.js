/**
 * corrector.js
 * @author Alexander Burtsev, http://burtsev.me, 2014
 * @license MIT
 */
;(function(window, document, undefined) {
	'use strict';

if ( !Array.prototype.indexOf ) {
	Array.prototype.indexOf = function indexOf(searchElement) {
		for (var array = this, index = 0, length = array.length; index < length; ++index) {
			if (array[index] === searchElement) {
				return index;
			}
		}

		return -1;
	};
}

	/**
	 * Entry point of corrector.js API
	 * @class
	 * @name Corrector
	 * @param {String} selector Selector of container with text
	 * @param {Object} rules Rules object
	 * @fires Corrector#corrector-start Fires before replacements
	 * @fires Corrector#corrector-end Fires after replacements
	 */
	function Corrector(selector, rules) {
		if ( !(this instanceof Corrector) ) {
			return new Corrector(selector, rules);
		}

		this.tree = Traversal(selector);
		this.rules = {};

		this
			.addRules(defaultRules)
			.addRules(rules);
	}

	Corrector.prototype = {
		/**
		 * Starts text replacements
		 * @returns {Corrector} Self object
		 */
		run: function() {
			var	_win = window.jQuery ? jQuery(window) : null,
				node, text,
				reNotWhipeSpace = /\S/;

			if ( _win ) {
				_win.trigger('corrector-start', this);
			}

			this.tree.reset();
			while ( (node = this.tree.next()) ) {
				if ( (text = node.nodeValue) && text.match(reNotWhipeSpace) ) {
					this.execute(node, text);
				}
			}

			if ( _win ) {
				_win.trigger('corrector-end', this);
			}

			return this;
		},

		/**
		 * Executes replacements for given text node
		 * @param {Object} node DOM text node (nodeType === 3)
		 * @param {Object} text Text of given node
		 * @returns {Corrector} Self object
		 */
		execute: function(node, text) {
			var rule;

			for (var name in this.rules) {
				if ( this.rules.hasOwnProperty(name) ) {
					rule = this.rules[name];
					if ( text.match(rule[0]) ) { // http://jsperf.com/match-vs-test
						node.nodeValue = text.replace(rule[0], rule[1]);
					}
				}
			}

			return this;
		},

		/**
		 * Adds custom rules to dictionary
		 * @param {Object} rules Rules object
		 * @returns {Corrector} Self object
		 */
		addRules: function(rules) {
			rules = rules || {};

			for (var name in rules) {
				if ( rules.hasOwnProperty(name) ) {
					this.rules[name] = rules[name];
				}
			}

			return this;
		},

		/**
		 * Clears dictionary with rules
		 * @param {Boolean} [useDefault=false] Use default rules if true
		 * @returns {Corrector} Self object
		 */
		clearRules: function(useDefault) {
			this.rules = useDefault ? defaultRules : {};
			return this;
		}
	};

	window.Corrector = Corrector;

	/**
	 * Acknowledgements for Artem Sapegin
	 * Based on https://github.com/sapegin/richtypo.js/blob/master/rules/ru.js
	 */
	var	_nbsp = String.fromCharCode(160),
		_months = 'января|февраля|марта|апреля|мая|июня|июля|августа|сентября|ноября|декабря',
		_prepos = 'а|ай|в|вы|во|да|до|ее|её|ей|за|и|из|их|к|ко|мы|на|не|ни|но|ну|о|об|ой|он|от|по|с|со|то|ты|у|уж|я',
		_particles = 'б|бы|ж|же|ли|ль';

	var defaultRules = {
		_dashes: [/\s—/g, _nbsp + '—'],
		_months: [new RegExp('(\\d)\\s(' + _months + ')', 'gi'), '$1' + _nbsp + '$2'],
		_prepos: [new RegExp('([^а-яёА-ЯЁ]|^)(' + _prepos + ')\\s', 'gi'), '$1$2' + _nbsp],
		_particles: [new RegExp('\\s(' + _particles + ')(?![-а-яё])', 'gi'), _nbsp + '$1'],
	};

	/**
	 * Traverses all child nodes for given selector and saves text nodes
	 * @class
	 * @name Traversal
	 * @param {String} selector
	 */
	function Traversal(selector) {
		if ( !(this instanceof Traversal) ) {
			return new Traversal(selector);
		}

		var nodes, i;

		this.selector = selector || 'body';
		this.cursor = 0;
		this.storage = [];

		try {
			nodes = document.querySelectorAll(this.selector);
		} catch(e) {
			nodes = [];
		}

		for (i = nodes.length; i--;) {
			this.traverse(nodes[i]);
		}
	}

	Traversal.prototype = {
		/** @property {Array} List of excluded tags */
		exclude: [
			'script',
			'style',
			'iframe',
			'img',
			'input'
		],

		/**
		 * Recursive traverse of all child nodes
		 * @param {Object} node DOM-element
		 * @param {Number} [nesting=0] Nesting level
		 */
		traverse: function(node, nesting) {
			var nodeName = node.nodeName.toLowerCase(),
				nodeType = node.nodeType;

			if ( this.exclude.indexOf(nodeName) !== -1 ) {
				return;
			}

			nesting = nesting || 0;
			if ( node.nodeType === 1 ) {
				for (var nodes = node.childNodes, i = nodes.length; i--;) {
					this.traverse(nodes[i], nesting + 1);
				}
			} else if ( nodeType === 3 ) {
				this.storage.push(node);
			}
		},

		/**
		 * Iterator for found text nodes
		 * @returns {Object} Next text node
		 */
		next: function() {
			var current = this.storage[this.cursor];
			this.cursor++;
			return current;
		},

		/**
		 * Resets iterator
		 */
		reset: function() {
			this.cursor = 0;
		}
	};

})(window, document);